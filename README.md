# Conversion binaire

Ce programme rassemble différentes fonctions sans rapport les unes avec les autres :
- une fonction de conversion d'un nombre de la base 2 ver la base 10 ;
- une fonction de calcul d'une moyenne pondérée ;
- une fonction de génération de mot de passe.
