import random

# T est la liste que l'utilisateur saisis qui va etre ensuite convertie par le programme
# a est ce que le programme va renvoyer. C'est le binaire sous forme décimal
def convertir(T):
    a = 0 #nom var explicite
    for i in range(len(T)):
        b = 2**((len(T)-1)-i)
        if T[i]==1:
            a = a+b
    return a
print("Exemple pour le premier programme 1:",convertir([1,0,1,0,0,1,1]))
print("2eme exemple pour le premier programme 1:",convertir([1,0,0,0,0,1,0]))

#liste qui est le parametre de la fonction moyenne. prend un compte une liste avec un couple "note,coefficient
#moyenne ponderée qui est ce que renvoie la fonction moyenne. est le resultat du calcul en fonction des notes et des coefficient selectionnés precedemment
def moyenne(liste):
    somme_notes_coefficients = 0.0
    somme_coefficients = 0

    for couple in liste:
        note = couple[0]
        coefficient = couple[1]

        somme_notes_coefficients += note * coefficient #calcul de moyenne ponderée
        somme_coefficients += coefficient

    if somme_coefficients == 0:
        return 0.0  # Évite une division par zéro si la liste n'est pas remplie
    else:
        moyenne_ponderee = somme_notes_coefficients / somme_coefficients
        return round(moyenne_ponderee, 2)  # Arrondi à 2 décimales apres la virgule

# Exemple d'utilisation
resultat = moyenne([(15, 2), (9, 1), (12, 3)])
print("l'exemple pour le programme 2 :",resultat)

#longueur prend un compte un entier positif qui va definir la longueur du mot de passe
#mdp est ce que le programme renvoie. C'est une chaine de caractère qui est composé du nombre de caractère définit grace a "longueur"
def mot_de_passe(longueur):
    chaine = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    mdp = ""

    for i in range(longueur):
        # Utilisation de randint pour obtenir un indice aléatoire
        indice_aleatoire = random.randint(0, len(chaine) - 1)
        mdp += chaine[indice_aleatoire]
    return mdp
print("exemple pour le programme 3:",mot_de_passe(6))
print("le 2eme exemple pour le programme 3:",mot_de_passe(10))